/**
 * merge eslint config objects
 * @param {object} base - base config
 * @param {object[]} configs - configs to be merged
 * @returns {object} merged eslint config
 */
const mergeConfig = (base, ...configs) =>
  configs.reduce(
    (final, config) =>
      Object.entries(config).reduce((acc, [key, val]) => {
        if (Array.isArray(acc[key])) {
          acc[key] = acc[key].concat(val);
        } else if (typeof acc[key] === 'object') {
          acc[key] = { ...acc[key], ...val };
        } else {
          acc[key] = val;
        }
        return acc;
      }, final),
    { ...base },
  );

const baseConfig = {
  parser: 'babel-eslint',
  extends: ['airbnb-base', 'plugin:jsdoc/recommended', 'plugin:prettier/recommended'],
  rules: {
    'no-inline-comments': 'error',
    'jsdoc/newline-after-description': 'off',
    'jsdoc/require-description': 'warn',
    'jsdoc/require-jsdoc': [
      'warn',
      {
        require: {
          ArrowFunctionExpression: true,
          FunctionDeclaration: true,
          FunctionExpression: true,
          MethodDefinition: true,
        },
      },
    ],
  },
};
const jestConfig = { extends: 'plugin:jest/recommended' };

module.exports = {
  ...baseConfig,
  root: true,
  overrides: [
    {
      ...mergeConfig(baseConfig, jestConfig),
      files: '**/*.test.js',
    },
  ],
};
