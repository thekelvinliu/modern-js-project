module.exports = {
  '*.js': ['yarn lint --fix', 'git add'],
  '*.{css,html,json,md,scss,yml}': ['yarn format', 'git add'],
};
